package Domini; 
 
import java.util.List; 
 
public abstract class Fitxa { 

	private Coordenada posicio;
	
	public Fitxa (Coordenada posicio) {
		this.posicio = posicio;
	}
	
	public Coordenada getPosicio () {
		return this.posicio;
	}
	
	public abstract List<Coordenada> getMovimentsPossibles(); 
}