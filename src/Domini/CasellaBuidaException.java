package Domini;

public class CasellaBuidaException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public CasellaBuidaException() {
		super();
	}
	
	public CasellaBuidaException(String s) {
		super(s);
	}
	
}
