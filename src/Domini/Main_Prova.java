package Domini;

import java.util.Scanner;

public class Main_Prova {

	public static void main (String[] args) throws IllegalArgumentException, CasellaBuidaException {
		
		JocDames jocProva = null;
		try {
			jocProva = new JocDames(12345);
		} catch (Exception e) {
			//Aqu� no hi haur� errors.
		}
		
		System.out.println("DAMES ART ONLINE");
		System.out.println("----------------");
		System.out.println("\nWelcome to Dames Art Online.");
		System.out.println("\nThis is your fucking tablero: every movement you make, it will be reprinted with it's new status.");
		System.out.println("Player One controls fixas Negras, Player Two controls fitxas Blancas");
		
		System.out.println();		
		veureTaulell(jocProva);
		
		boolean play = true;
		Scanner scan;
		String command;
		while (play) {
			System.out.println();
			if (jocProva.getTorn())	{
				System.out.print("Player One, is your turn.");
			} else {
				System.out.print("\nPlayer Two, is your turn.");
			}
			System.out.println("\nTo make a move, write [move] and press enter. To exit the game, write [exit].");
			scan = new Scanner(System.in);
			command = scan.nextLine();
			switch (command) {
			case ("move"):
				System.out.println();
				int row0, col0, row1, col1;
				System.out.println("To select the fitxa you want to move...");
				System.out.println("Introduce the current row of the fitxa: ");
				row0 = scan.nextInt();
				System.out.print("Introduce the current column of the fitxa: ");
				col0 = scan.nextInt();
				try {
					while (jocProva.getMovimentsLegals(row0, col0) == null) {
						System.out.println("This fitxa can not be moved.");
						System.out.println("To select the fitxa you want to move...");
						System.out.println("Introduce the current row of the fitxa: ");
						row0 = scan.nextInt();
						System.out.print("Introduce the current column of the fitxa: ");
						col0 = scan.nextInt();
					}
				} catch (Exception e) {
					e.getCause();
				}
				System.out.println();
				System.out.println("These are your moves:");
				for (Coordenada c : jocProva.getMovimentsLegals(row0, col0)) {
					System.out.print("| "+c.getFila()+" , "+c.getColumna()+" ");
				}
					System.out.println("|");
				System.out.println();
				System.out.println("To move the fitxa...");
				System.out.println("Introduce the destiny row of the fitxa: ");
				row1 = scan.nextInt();
				System.out.print("Introduce the destiny column of the fitxa: ");
				col1 = scan.nextInt();
				try {
					while (!jocProva.getMovimentsLegals(row0, col0).contains(new Coordenada(row1, col1))) {
						System.out.println("This fitxa cannot be moved to that position.");
						System.out.println("To move the fitxa...");
						System.out.println("Introduce the destiny row of the fitxa: ");
						row1 = scan.nextInt();
						System.out.print("Introduce the destiny column of the fitxa: ");
						col1 = scan.nextInt();
					}
				} catch (Exception e) {
					System.out.println(e);
				}
				try {
					jocProva.moureFitxa(row0, col0, row1, col1);
				} catch (Exception e) {
					System.out.println(e);
				}
				jocProva.changeTorn();
				break;
			case("exit"):
				play = false;
				break;
			}
		}
	}
	
	private static void veureTaulell(JocDames joc) {
		for (int i = 0; i < joc.getTaulell().length; i++) {
			System.out.println("|---|---|---|---|---|---|---|---|");
			for (int j = 0; j < joc.getTaulell()[i].length; j++) {
				Fitxa fitxa = joc.getTaulell()[i][j];
				if (fitxa == null) {
					System.out.print("|   ");
				} else if (joc.fitxaEsNegra(fitxa)) {
					System.out.print("| N ");
				} else {
					System.out.print("| B ");
				}
			}
			System.out.println("| "+i);
		}
		System.out.println("|---|---|---|---|---|---|---|---|");
		System.out.println("  0   1   2   3   4   5   6   7  ");
	}
}
