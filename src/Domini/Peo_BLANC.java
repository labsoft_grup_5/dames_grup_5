package Domini;

import java.util.ArrayList;
import java.util.List;

public class Peo_BLANC extends Fitxa {

	private Coordenada posicio;
	
	public Peo_BLANC(Coordenada posicio) {
		super(posicio);
	}
	
	public List<Coordenada> getMovimentsPossibles() {
		List<Coordenada> movimentsPossibles = new ArrayList<Coordenada>();
		try {
			//Diagonal dreta amunt
			movimentsPossibles.add(new Coordenada(this.posicio.getFila()-1, this.posicio.getColumna()+1));
		} catch (Exception e) {
			//Ignore this coordenada.
		} try {
			//Diagonal esquerra amunt
			movimentsPossibles.add(new Coordenada(this.posicio.getFila()-1, this.posicio.getColumna()-1));
		} catch (Exception e) {
			//Ignore this coordenada.
		}
		return movimentsPossibles;
	}

}