package Domini;

public class IllegalCoordenadaException extends Exception{

	private static final long serialVersionUID = 1L;

	public IllegalCoordenadaException() {
		super();
	}
	
	public IllegalCoordenadaException(String s) {
		super(s);
	}
	
}
