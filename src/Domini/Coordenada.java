package Domini;

public class Coordenada implements Comparable<Coordenada>{

	private int fila;
	private int columna;
	
	public Coordenada (int fila, int columna) throws IllegalCoordenadaException {
		if (fila < 0 || fila >= 8 || columna < 0 || columna >= 8) {
			throw new IllegalCoordenadaException("No s'ha pogut crear la Coordenada.");
		}
		this.fila = fila;
		this.columna = columna;
	}
	
	public int getFila () {
		return this.fila;
	}
	
	public void setFila (int fila) {
		this.fila = fila;
	}
	
	public int getColumna () {
		return this.columna;
	}
	
	public void setColumna (int columna) {
		this.columna = columna;
	}

	@Override
	public int compareTo(Coordenada c) {
		if (c instanceof Coordenada) {
			if (this.fila == ((Coordenada)c).getFila() && this.columna == ((Coordenada)c).getColumna()) {
				return 0;
			}
		}
		return -1;
	}
	
}