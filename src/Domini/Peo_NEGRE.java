package Domini;

import java.util.ArrayList;
import java.util.List;

public class Peo_NEGRE extends Fitxa {
	
	private Coordenada posicio;
	
	public Peo_NEGRE(Coordenada posicio) {
		super(posicio);
	}
	
	public List<Coordenada> getMovimentsPossibles() {
		List<Coordenada> movimentsPossibles = new ArrayList<Coordenada>();
		try {
			//Diagonal dreta avall.
			movimentsPossibles.add(new Coordenada(this.posicio.getFila()+1, this.posicio.getColumna()+1));
		} catch (Exception e) {
			//Ignore this coordenada.
		} try {
			//Diagonal esquerra avall.
			movimentsPossibles.add(new Coordenada(this.posicio.getFila()+1, this.posicio.getColumna()-1));
		} catch (Exception e) {
			//Ignore this coordenada.
		}
		return movimentsPossibles;
	}

}
