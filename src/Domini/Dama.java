package Domini;

import java.util.ArrayList;
import java.util.List;

public class Dama extends Fitxa {

	private Coordenada posicio;
	private boolean esNegra;
	
	public Dama(Coordenada posicio, boolean esNegra) {
		super(posicio);
		this.esNegra = esNegra;
	}
	
	public boolean esNegra() {
		return this.esNegra;
	}
	
	public List<Coordenada> getMovimentsPossibles() {
		List<Coordenada> movimentsPossibles = new ArrayList<Coordenada>();
		try {
			//Diagonal dreta amunt
			movimentsPossibles.add(new Coordenada(this.posicio.getFila()-1, this.posicio.getColumna()+1));
		} catch (Exception e) {
			//Ignore this coordenada.
		} try {
			//Diagonal esquerra amunt
			movimentsPossibles.add(new Coordenada(this.posicio.getFila()-1, this.posicio.getColumna()-1));
		} catch (Exception e) {
			//Ignore this coordenada.
		} try {
			//Diagonal dreta avall.
			movimentsPossibles.add(new Coordenada(this.posicio.getFila()+1, this.posicio.getColumna()+1));
		} catch (Exception e) {
			//Ignore this coordenada.
		} try {
			//Diagonal esquerra avall.
			movimentsPossibles.add(new Coordenada(this.posicio.getFila()+1, this.posicio.getColumna()-1));
		} catch (Exception e) {
			//Ignore this coordenada.
		}
		return movimentsPossibles;
	}
	
}
