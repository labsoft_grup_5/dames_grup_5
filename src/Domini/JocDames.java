package Domini;

import java.util.ArrayList;
import java.util.List;

public class JocDames {
	
	private Integer idPartida;
	private Fitxa[][] taulell;
	private boolean torn = true; //Comen�a sempre el jugador 1, que juga amb les fitxes negres.
	
	public JocDames (int idPartida) throws Exception {
		this.idPartida = idPartida;
		
		//inicialitza taulell i l'emplena
		this.taulell = new Fitxa[8][8];
		for (int i = 0; i < this.taulell.length; i++) {
			for (int j = 0; j < this.taulell[i].length; j++) {
				if (i % 2 == 0 && j % 2 == 0 || i % 2 == 1 && j % 2 == 1) {
					if (i < 3) {
						this.taulell[i][j] = new Peo_NEGRE(new Coordenada (i, j));
					}
					if (i > 4) {
						this.taulell[i][j] = new Peo_BLANC(new Coordenada (i, j));
					}
				}
			}
		}
	}
	
	public List<Coordenada> getMovimentsLegals (int fila, int columna) throws CasellaBuidaException, IllegalArgumentException {
		/*Calcula els moviments legals segons el context de la fitxa que es vol moure, basant-se en els
		  moviments possibles de la mateixa. Tamb� busca si hi han salts (menjar fitxa rival) possibles,
		  i en tal cas nom�s contempla aquests com a legals, responent a la obligatorietat d'aquests. */
		//Si el taulell �s buit en la posici� subministrada i encara no hem trobat cap moviment legal...
		if (this.taulell[fila][columna] == null) {
			//Suspenem l'execuci� del m�tode ja que no hi ha res per moure.
			throw new CasellaBuidaException("Aquesta casella �s buida.");
		}
		//En cas contrari prenem la refer�ncia de la fitxa amb la posici� subministrada.
		Fitxa fitxa0 = this.taulell[fila][columna];
		//Si aquesta fitxa no �s del jugador actual...
		if (!fitxaCorresponJugador(fitxa0)) {
			//Suspenem l'execuci� del m�tode ja que no la ha de poder moure.
			throw new IllegalArgumentException("Aquesta fitxa no es teva.");
		}
		//En cas contrari, creem una llista per guardar moviments Legals.
		List<Coordenada> movimentsLegals = new ArrayList<Coordenada>();
		//Creem un boole� per saber si hem saltat.
		boolean potSaltar = false;
		//Mentre quedin moviments possibles sense evaluar (Coordenades de dest� posibles)...
		for (Coordenada c : fitxa0.getMovimentsPossibles()) {
			//Si aquest moviment corresp�n a una casella buida...
			if (this.taulell[c.getFila()][c.getColumna()] == null) {
				//L'afegim com moviment legal.
				movimentsLegals.add(c);
			//En cas contrari...
			} else {
				//Prenem la refer�cia de la fitxa ubicada a la Coordenada que estem evaluant.
				Fitxa fitxa1 = this.taulell[c.getFila()][c.getColumna()];
				//Si aquesta fitxa no es del jugador actual i movent-se una casella en la mateixa diagonal troba una posici� buida...
				if (!fitxaCorresponJugador(fitxa1) && this.taulell[c.getFila()+calcularDistancia(fila, c.getFila())][c.getColumna()+calcularDistancia(columna, c.getColumna())] == null) {
					//Si aquesta coordenada es troba dins dels l�mits del taulell...
					try {
						//L'afegim als moviments legals.
						movimentsLegals.add(new Coordenada(c.getFila()+calcularDistancia(fila, c.getFila()), c.getColumna()+calcularDistancia(columna, c.getColumna())));
						//I deixem const�ncia de que es pot saltar.
						potSaltar = true;
					} catch (IllegalCoordenadaException e) {
						//Ignorem les posicions il�legals
					}
				}
			}
		}
		//Si es pot saltar...
		if (potSaltar) {
			//Depurem els moviments legals que corresponen a els possibles de la fitxa, ja que corresponen a avan�ar una diagonal, i volem obligar a saltar.
			for (Coordenada c : fitxa0.getMovimentsPossibles()) {
				if (movimentsLegals.contains(c)) {
					movimentsLegals.remove(c);
				}
			}
			
		}
		return movimentsLegals;
	}
	
	public void moureFitxa(int filaOrigen, int columnaOrigen, int filaDesti, int columnaDesti) throws Exception {
		List<Coordenada> movimentsLegals = getMovimentsLegals(filaOrigen, columnaOrigen);
		for (Coordenada c : movimentsLegals) {
			if (c.getFila() == filaDesti && c.getColumna() == columnaDesti) {
				if (calcularDistancia(filaOrigen, filaDesti) == 2) {
					this.taulell[filaOrigen+calcularDistancia(filaOrigen, filaDesti)/2][columnaOrigen+calcularDistancia(columnaOrigen, columnaDesti)/2] = null;
				}
				Fitxa fitxa = this.taulell[filaOrigen][columnaOrigen];
				if (fitxaEsNegra(fitxa) && filaDesti == 7 || !fitxaEsNegra(fitxa) && filaDesti == 0) {
					this.taulell[filaDesti][columnaDesti] = new Dama(new Coordenada(filaDesti, columnaDesti), torn);
				}
				this.taulell[filaOrigen][columnaOrigen] = null;
			}
		}
	}
	
	public boolean getTorn() {
		return this.torn;
	}
	
	public void changeTorn() {
		this.torn = !this.torn;
	}

	public Fitxa[][] getTaulell() {
		return this.taulell;
	}
	
	private int calcularDistancia(int origen, int desti) {
		return desti-origen;
	}
	
	public boolean fitxaEsNegra(Fitxa fitxa) {
		if (fitxa instanceof Peo_NEGRE || fitxa instanceof Dama && ((Dama)fitxa).esNegra()) {
			return true;
		}
		return false;
	}
	
	private boolean fitxaCorresponJugador(Fitxa fitxa) {
		return fitxaEsNegra(fitxa) == this.torn;
	}
	
}