package Aplicacio;

import Persistencia.PersistenciaIniciaCreaSessio;
import Persistencia.SessioObertaException;



public class IniciaCrearSessio {
	
	private PersistenciaIniciaCreaSessio persistencia;
		
	public Sessio IniciaSessio(String usuari, String contrasenya) throws UsuariContrasenyaException, SessioObertaException {
		Sessio objetoUsuari = null;
		if (usuari.contains(";")||usuari.contains("\"")) throw new UsuariContrasenyaException();
		if (contrasenya.contains(";")||contrasenya.contains("\"")) throw new UsuariContrasenyaException();
		String idSessio = persistencia.login(usuari, contrasenya);
		objetoUsuari = new Sessio(idSessio); 
		return objetoUsuari;
	}
	
	public void CrearSessio(String usuari, String contrasenya) throws NomEnUsException, InvalidStringException { 
		if (usuari.contains(";")||usuari.contains("\"")) throw new InvalidStringException();
		if (contrasenya.contains(";")||contrasenya.contains("\"")) throw new InvalidStringException();
		persistencia.CrearSessio(usuari, contrasenya);
		
	}
}
