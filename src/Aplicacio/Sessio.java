package Aplicacio;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Persistencia.PersistenciaConsulta;

public class Sessio {
	
	private String idSessio;
	private PersistenciaConsulta persistencia;
	
	public Sessio(String idSessio) {
		this.idSessio = idSessio;
	}
	public List<String> GetCandidatsSol(){
		return persistencia.GetCandidatsSol(idSessio);
	}
	
	public Map<String, Boolean> GetPartidas(){
		//Map<String, Boolean> PartidesEstat = new HashMap<String, Boolean>();
		
		return persistencia.GetPartidas(idSessio);
	}
	
}
